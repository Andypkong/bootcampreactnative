import React, { Component } from 'react';
import {Image, TouchableOpacity View, StyleSheet, FlatList} from 'react-native';
import {Icon} from 'react-native-vector-icons/MaterialIcons';
import data from './data.json'

export default class App extends Component {
    render() {
        return (
            <View style={StyleSheet.Component}>
                <View style={StyleSheet.navbar}>
                    <image source={require('./Image/logo.png')} 
                        style=
                        {
                            {
                            height:20,
                            width :90
                            }
                        }>
                    </image>
                    <View style={StyleSheet.rightNav}>
                        <TouchableOpacity>
                            <Icon name="search" size={20} style={StyleSheet.navItem}/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Icon name="account-circle" size={20} style={StyleSheet.navItem}/>
                        </TouchableOpacity>
                        </View>
                    </View>
                    <View>
                        <FlatList 
                        data={data.items}
                        renderItem={(video)=><VideoItem/>}
                        />
                    </View>
            </View>
        )
    }
}

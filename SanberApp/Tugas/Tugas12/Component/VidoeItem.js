import React, { Component } from 'react'
import { View } from 'react-native'

export default class VidoeItem extends Component {
    render() {
        let video = this.props.video
        return (
            <View style={styles.container}>
                <Image source={{uri: video.snippet.thumnails.url}} style={{height:200}}/>
                <View style = {styles.descContainer}>
                    <Image source={{uri:"https://randomuser.me/api/portraits/men/0.jpg"}} style={{width: 50, height:50, borderRadius:50 /2}}/>
                    <View style={styles.videoDetail}>
                        <Text numberOfLines={2} style={styles.videoTitle}>
                            {
                                video.snippet.title
                            }
                        </Text>
                        <Text numberOfLines={2} style={styles.videoTitle}>
                            {
                                video.snippet.title
                            }
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

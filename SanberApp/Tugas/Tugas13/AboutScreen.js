import React from 'react'
import 
    { 
        Image, 
        StyleSheet, 
        Text, 
        View, 
        Platform, 
        TouchableOpacity, 
        ScrollView,
        TextInput,
        Button 
    } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const AboutScreen = () => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.textHead}>Tentang Saya</Text>
                <FontAwesome5 
                name="user-circle"
                size={100} solid
                color="#EFEFEF"
                style={styles.icon}></FontAwesome5>
                <Text style={styles.textName}>Andy Pratama</Text>
                <Text style={styles.textJob}>React Native Developer</Text>
                <View style={styles.boxPortoOuter}>
                    <Text style={styles.textPorto}>Portofolio</Text>
                    <View style={styles.boxPotoInner}>
                        <View>
                            <FontAwesome5 
                            name="gitlab"
                            size={35} 
                            color="#3EC6FF"
                            style={styles.icon}></FontAwesome5>
                            <Text style={styles.textPorto1}>@andypkong</Text>
                        </View>
                        <View>
                            <FontAwesome5 
                            name="github"
                            size={35} 
                            color="#3EC6FF"
                            style={styles.icon}></FontAwesome5>
                            <Text style={styles.textPorto1}>@andypkong</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.contactBox}>
                    <Text style={styles.textPorto}>Hubungi Saya</Text>
                    <View style={styles.innerBoxCtc}>
                        <View style={styles.innerBoxCtcPerson}>
                            <FontAwesome5 
                            name="facebook"
                            size={35} 
                            color="#3EC6FF"
                            style={styles.icon}></FontAwesome5>
                            <Text style={styles.textContact}>@andypkong</Text>
                        </View>
                        <View style={styles.innerBoxCtcPerson}>
                            <FontAwesome5 
                            name="instagram"
                            size={35} 
                            color="#3EC6FF"
                            style={styles.icon}></FontAwesome5>
                            <Text style={styles.textContact}>@andypkong</Text>
                        </View>
                        <View style={styles.innerBoxCtcPerson}>
                            <FontAwesome5 
                            name="twitter"
                            size={35} 
                            color="#3EC6FF"
                            style={styles.icon}></FontAwesome5>
                            <Text style={styles.textContact}>@andypkong</Text>
                        </View>
                    </View>
                </View>
            </View>
    </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create(
    {
        container:
        {
          marginTop:80,
        
        },
        textHead:
        {
            color:"#003366",
            fontSize:24,
            fontWeight:'bold',
            marginVertical:15,
            textAlign:'center'
        },
        textName:
        {
            color:"#003366",
            fontSize:16,
            fontWeight:'bold',
            marginVertical:1,
            textAlign:'center'
        },
        textJob:
        {
            color:"#3EC6FF",
            textAlign:'center'
        },
        textPorto:
        {
            color:"#003366",
            fontSize:16,
            padding:3
        },
        boxPortoOuter:
        {
            backgroundColor:"#EFEFEF",
            marginVertical:15,
            padding:10,
            borderRadius:20,
            marginHorizontal:20
        },
        boxPotoInner:
        {
            borderTopWidth:2,
            borderTopColor:"#003366",
            flexDirection:'row',
            justifyContent:"space-around",
            padding:10
        },
        icon:
        {
            textAlign:'center'
        },
        contactBox:
        {
            backgroundColor:"#EFEFEF",
            marginVertical:15,
            padding:10,
            borderRadius:20,
            marginHorizontal:20
        },
        innerBoxCtc:
        {
            borderTopWidth:2,
            borderTopColor:"#003366",
            padding:10,
            alignItems:'center'
        },
        innerBoxCtcPerson:
        {
            flexDirection:'row',
            padding:10
        },
        textContact:
        {
            justifyContent:'center',
            marginLeft:10,
            marginTop:7
        }

    })

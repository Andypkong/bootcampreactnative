import React from 'react'
import 
    { 
        Image, 
        StyleSheet, 
        Text, 
        TextInput, 
        View, 
        Platform, 
        TouchableOpacity, 
        Button, 
        KeyboardAvoidingView,
        ScrollView 
    } from 'react-native'

const RegisterScreen = () => {
    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding":"height"}
        Style={styles.container}>
            <ScrollView>
                <View style={styles.containerView}>
                    <Image source={require('../Tugas13/assets/logo.png')}></Image>
                    <Text style={styles.registxt}>Register</Text>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Username</Text>
                        <TextInput style={styles.input}></TextInput>
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Email</Text>
                        <TextInput style={styles.input}></TextInput>
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}></TextInput>
                    </View>
                    <View style={styles.forminput}>
                        <Text style={styles.formtext}>Ulangi Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}></TextInput>
                    </View>
                    <View style={styles.kotakbtn}>
                        <TouchableOpacity style={styles.btnregister}>
                            <Text style={styles.textbtn}>Daftar</Text>
                        </TouchableOpacity>
                        <Text style={styles.autotext}>Atau</Text>
                        <TouchableOpacity style={styles.btnlogin}>
                            <Text style ={styles.textbtn}>Masuk</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default RegisterScreen

const styles = StyleSheet.create(
    {
        container: 
        {
           flex: 1,
        },
        registxt:
        {
            fontSize: 28,
            textAlign: 'center',
            marginTop:50,
            color:"#003366",
            marginVertical:50
        },
        formtext:
        {
            color:"#003366"
        },
        autotext:
        {
            color: "#3EC6FF",
            fontSize:25,
            marginHorizontal:143
        },
        forminput:
        {
            marginHorizontal:30,
            marginVertical:8,
            alignContent:'center',
            width:300
        },
        input:
        {
            borderColor: "#3EC6FF",
            borderWidth: 1,
            height :50,
            textAlign :'center',
            padding: 15
        },
        kotakbtn:
        {
            marginTop:20
        },
        textbtn:
        {
            color:"#FFFFFF",
            fontWeight:"bold"
        },
        btnlogin:
        {
            backgroundColor:"#3EC6FF",
            alignItems:'center',
            borderRadius:30,
            padding:15,
            fontSize:20,
            width: 140,
            marginHorizontal: 100,
            marginVertical:10

        },
        btnregister:
        {
            backgroundColor:"#003366",
            alignItems:'center',
            borderRadius:30,
            padding:15,
            fontSize:20,
            width: 140,
            marginHorizontal: 100,
            marginVertical:10
        }
        
    })


//Soal 1
console.log("=====Soal 1=====")
const golden = () => console.log("this is golden!!"); 
golden()
console.log("=====Done=====")
console.log("\n");

//Soal 2
console.log("=====Soal 2=====")
const newFunction = ((firstName, lastName) => console.log(`${firstName} ${lastName}`))
newFunction("William", "Imoh") 
console.log("=====Done=====")
console.log("\n");

//Soal 3
console.log("=====Soal 3=====")
const newObject = 
{
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName,lastName,destination,occupation,spell);
console.log("=====Done=====")
console.log("\n");

//Soal 4
console.log("=====Soal 4=====")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

console.log(combined)
console.log("=====Done=====")
console.log("\n");

//Soal 5
console.log("=====Soal 5=====")
const planet = "earth"
const view = "glass"
const before = 
`Lorem  ${view}  dolor sit amet, 
consectetur adipiscing elit, ${planet}  do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
 
console.log(before) 
console.log("=====Done=====")
console.log("\n");
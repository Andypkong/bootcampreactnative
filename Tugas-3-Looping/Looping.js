//Soal 1 (while)=======================
var angka=1

//Looping While ++
console.log("LOOPING PERTAMA");
while(angka<21)
{
    angka++;
    if(angka%2==0)
    {
        console.log(angka+' - I love coding');
    }
    else 
    {

    }
    
}
console.log(" ");

//Looping While --
console.log("LOOPING KEDUA");
while(angka>1)
    {
        angka--;
        if(angka%2==0)
        {
            console.log(angka+' - I will become a mobile developer');
        }
        else
        {

        }
    }
//====================================Done
console.log(" ");

//Soal 2 (For)============================
//Looping For
for(var angka2=1; angka2<21;angka2++)
{
    if(angka2%2==0)
    {
        console.log(angka2+" - Berkualitas");
    }
    else if(angka2%3==0)
    {
        console.log(angka2+" - I Love Coding")
    }
    else
    {
        console.log(angka2+" - Santai");
    }
}
//====================================Done
console.log(" ");

//Soal 3 (square pattern)=================
var rows = 4;
var colloumn=8;
var shape=" ";
for(var i=1; i<=rows; i++)
{
    for(var j=1; j<=colloumn; j++)
    {
        shape+="#";
    }
    console.log(shape);
    shape=" ";
}
//====================================Done
console.log(" ");

//Soal 4 (triangle pattern)=================
var rows = 7;
var colloumn=7;
var shape='';
for(var i=1; i<=rows; i++)
{
    for(var j=1; j<=i; j++)
    {
        shape+="#";
    }
    console.log(shape);
    shape="";
}
//====================================Done
console.log(" ");

//Soal 5 (chess pattern)=================
var rows = 8;
var colloumn=8;
var shape='';
for(var i=1; i<=rows; i++)
{
    if(i%2!=0)
    {
        for(var j=1; j<=colloumn; j++)
        {
            if(j%2!=0)
            {
                shape +=" ";
            }
            else
            {
                shape +="#";
            }

        }
    }
    else
    {
        for(var j=1; j<=colloumn; j++)
        {
            if(j%2!=0)
            {
                shape+="#";
            }
            else
            {
                shape+=" ";
            }
            
        }
    }
    
    console.log(shape);
    shape="";
}
//====================================Done
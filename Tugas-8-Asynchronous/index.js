var readBooks = require('./callback.js')
 
var books = 
[
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let startTime = 10000
let bookName = 0
function calculate ()
{
    readBooks(startTime,books[bookName],function(timeLeft)
    {
        startTime=timeLeft
        bookName++
        if (bookName<books.length) {
            calculate()
        }
    })
}
calculate() 
//soal 1
console.log("=====Soal 1=====")
//release 0
class Animal
{
    constructor(name)
    {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name()          { return this._name; }
    set name(x)         { return this._name(x); }
    get legs()          { return this._legs; }
    set legs(amount)    { return this._legs=amount; }
    get cold_blooded()  { return this._cold_blooded; }
    set cold_blooded(x) { return this._cold_blooded=x; }
}

var sheep= new Animal("Shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
console.log(" ");
//Release 1
class Ape extends Animal
{
    constructor(name,legs_Amount)
    {
        super(name)
        this._legs = legs_Amount   
    }
    yell()
    {
        return "Auooooo"
    }
    
}
class Frog extends Animal
{
    constructor(name,legs_amount)
    {
        super(name)
        this._legs=legs_amount
    }
    jump()
    {
        return "Hop hop hop"
    }
}

var sungokong = new Ape("kera sakti",2)
sungokong.yell() 
 
var kodok = new Frog("buduk",2)
kodok.jump() 

console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);
console.log(" ");
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded);
console.log("===== Done =====")
console.log("\n")
console.log("=====Soal 2=====")
class Clock 
{

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
console.log("===== Done =====")
console.log("\n")